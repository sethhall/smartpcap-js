'use strict';

//struct PktMsg {
//	uint32_t magicNumber;	// 'spcp' - could indicate version, or allow other packet types on the connection
//	uint32_t curSize;	// payload size
//	SPCmd cmd;
//	uint32_t numPkts;// Number of packets stored
//	u_char uidStr[32];//Base62 UID for current flow.
//	uint8_t buffers[0];		// A sequence of PcapPacket
//}	__attribute__ ((packed));
import fs from 'fs'
import net from 'net'
import path from 'path'
import process from 'process'

import config from 'config'
import byt from 'byt'
import sqlite3 from 'sqlite3'
import { open } from 'sqlite'

import { tracking_db_file, find_first_segment, get_segment_filename, get_offsets_db } from './tracking-db.js'

var SHUTTING_DOWN=false

try { 
    fs.mkdirSync(config.get("storage.data_dir"))
} catch (err) {
    if ( err && err.code != 'EEXIST' ) {
        console.log(`error when trying to create data storage directory: ${err.type}`)
    }
}

var tracking_db = await open({ filename: tracking_db_file, driver: sqlite3.Database})
await tracking_db.run('PRAGMA JOURNAL_MODE = WAL;')
await tracking_db.run("CREATE TABLE IF NOT EXISTS segment_files ( segment INT, opened REAL DEFAULT ( julianday('now') ), closed DATE, size INT, packets INT, current BOOLEAN, hot BOOLEAN );")
console.log("opened the tracking database")

var current_offsets_db = null
var segment_file = null
var insert_stmt = null
var file_offset = 0
var file_num_packets = 0

var current_segment = await find_first_segment(tracking_db)
console.log(`Starting with segment ${current_segment}`)

var conn_num = 0
var unixServer = net.createServer( async function(client) {

    // Do something with the client connection
    console.log("connection")
    client.saved_buffer = null
    client.synced = false
    var queued = 0
    var chunk = 0

    const this_conn_num = conn_num++

    client.on('data', (x) => {
        //console.log("got data!")

        if (!segment_file || file_offset > byt(config.get("storage.segment_size"))) {
            current_segment++
            //console.log(`new current segment: ${current_segment}`)

            // Update the tracking DB to indicate old file sizes and a new file
            try { 
                if ( current_segment-1 > 0 ) {
                    tracking_db.run("UPDATE segment_files SET size=?, packets=?, current=0, closed=julianday('now') WHERE segment=?", [file_offset, file_num_packets, current_segment-1]);
                } 
                tracking_db.run("INSERT INTO segment_files (segment, size, packets, current, hot) VALUES (?,0,0,1,1)", [current_segment]);
            } catch (e) {
                console.log("SQL failure? " + e)
            }

            if ( segment_file ) {
		console.log(`closing a segment file: ${current_segment-1}`)
                //segment_file.close()
            }
            let segment_filename = get_segment_filename(current_segment)
            //segment_file = fs.createWriteStream(segment_filename)
           segment_file = fs.openSync(segment_filename, 'w')
            if ( current_offsets_db ) {
                insert_stmt.finalize()
                insert_stmt = null
                current_offsets_db.close()
            }
            current_offsets_db = get_offsets_db(current_segment, true)
            file_offset = 0
            file_num_packets = 0
        }

        chunk++
        //var buffer_bulking = Buffer.alloc(1024 * 1024)

        try {
            if (client.saved_buffer != null) {
                //console.log(`joining buffers to form a buffer of ${x.length} bytes`)
                x = Buffer.concat([client.saved_buffer, x])
            }

            var offset = 0
            while (offset < x.length) {
                //var msg_prefix  = ""
                if (!client.synced) {
                    var next_msg_offset = x.indexOf("spcp", offset)
                    if (next_msg_offset == -1) {
                        // the scenario where this happens is if we have a chunk with only "sp" in it, so we buffer
                        client.saved_buffer = x.slice(offset)
                        return
                    }
                    if (next_msg_offset != offset) {
                        console.log(`there was a weird gap in messages? ${next_msg_offset - offset}`)
                        //console.log(x.slice(offset), offset + (next_msg_offset - offset))
                    }
                    offset += (next_msg_offset - offset)
                    console.log("client is synced!")
                    client.synced = true
                }
                //console.log(`== ${this_conn_num} == ${chunk} == offset: ${offset}  next_msg_offset: ${next_msg_offset} -- remaining bytes ${x.length - offset}`)

                //msg_prefix = x.slice(offset,offset+4)
                offset += 4

                //if ( msg_prefix != "spcp" ) {
                //  console.log(`This needs to have found spcp, wtf!?!?!!?!?!?!?!?!?! (${msg_prefix}) `)
                //}

                if (offset + 4 > x.length) {
                    client.saved_buffer = x.slice(offset - 4)
                    return
                }
                const curSize = x.readUInt32LE(offset)
                offset += 4

                if (offset + (curSize - 4 - 4) > x.length) {
                    //console.log(`buffering data not enough to parse all packets curSize: ${curSize}, x.length: ${x.length}, offset: ${offset}, whatever: ${x.length - offset - 4-4}`)
                    client.saved_buffer = x.slice(offset - 8)
                    return
                } else {
                    client.saved_buffer = null
                }

                // skip command
                offset += 4

                const numPkts = x.readUInt32LE(offset)
                offset += 4

                const uid = x.slice(offset,x.indexOf("\0", offset)).toString('utf8')
                offset += 32

                const packets = x.slice(offset, offset + curSize - 32 - 4 - 4 - 4 - 4)
                offset += curSize - 32 - 4 - 4 - 4 - 4

                //console.log(`conn:${this_conn_num} ~ chunk:${chunk} = one chunk of data of ${numPkts} packets and ${packets.length} bytes - uid: ${uid}`)

                if (insert_stmt && queued > 100) {
                    insert_stmt.finalize()
                    insert_stmt = null
                    queued = 0
                }

                if (! insert_stmt  && current_offsets_db) {
                    queued = 0
                    insert_stmt = current_offsets_db.prepare("INSERT INTO packets VALUES (?,?,?,?,?)");
                }

                queued++
                //console.log("preparing to write data")

                if ( SHUTTING_DOWN ) {
                    return
                }
                //console.log(`${queued} -> wrote ${numPkts} packets (size: ${packets.length}) from ${uid} at offset ${file_offset}`)
                //segment_file.write(packets)
                fs.writeSync(segment_file, packets, 0, packets.length, file_offset)
                insert_stmt.run(uid, current_segment, file_offset, packets.length, numPkts)

                file_offset += packets.length
                file_num_packets += numPkts
            }

            client.saved_buffer = null
        } catch (err) {
            console.log("barfing")
            console.log(err)
            process.exit(1)
        }
    });
});


async function cleanDisk() {
  if ( SHUTTING_DOWN ) return

  console.log("kicking off disk cleanup")
  tracking_db.get('SELECT SUM(size) AS total_segments_size FROM segment_files WHERE hot=1', [], (err, row) => {

    let total_hot = row.total_segments_size
    //console.log(`total hot data: ${total_hot}   max allow utilization: ${config.get("storage.max_disk_utilization")}`)
    if ( total_hot > byt(config.get("storage.max_disk_utilization")) ) {
      var done_finding_files_to_delete = false
      tracking_db.each('SELECT segment, size FROM segment_files WHERE hot=1 ORDER BY segment ASC', [], (err, sf_row) => {
          let segment = sf_row.segment
          if ( ! done_finding_files_to_delete ) {
              //console.log(`going to delete segment: ${segment}`)
              total_hot = total_hot - sf_row.size
              if ( total_hot < byt(config.get("storage.max_disk_utilization")) ) {
                  done_finding_files_to_delete = true
              }

              tracking_db.run("DELETE FROM segment_files WHERE segment=?", [segment]);
              let seg_filename = get_segment_filename(segment)
              let files_to_delete = [
                seg_filename,
                seg_filename + '.db',
                seg_filename + '.db-wal',
                seg_filename + '.db-shm'
              ];

              files_to_delete.forEach( (file_to_delete) => {
                  fs.unlink(file_to_delete, (err) => {
                      // We don't necessarily care if the file is gone.  It could be a race condition with the database update query.
                      if ( err && err.code != 'ENOENT' ) {
                          console.log(`Error when attempting to delete segment file: ${err}`)
                      }
                  })
              })
          } else {
              //console.log(`Leaving segment ${segment}`)
          }
      })
    }
  })
}
setInterval(cleanDisk, config.get("storage.cleanup_interval") * 1000);


async function updateStats() {
    try {
        tracking_db.run("UPDATE segment_files SET size=?, packets=?, current=0, closed=julianday('now') WHERE segment=?", [file_offset, file_num_packets, current_segment]);
    } catch (e) {
        console.log("SQL failure? " + e)
    }
}
setInterval(updateStats, 3000);





try {
    fs.unlinkSync(config.get("zeek.socket_file"))
} catch (e) { 
    // don't care 
}
unixServer.listen(config.get("zeek.socket_file"))


process.on('SIGINT', async function(code) {
    SHUTTING_DOWN = true
    unixServer.close( () => {
        console.log("Closed the packet socket with Zeek")
    })
    console.log('Process exit event with code: ', code);
    tracking_db.run("UPDATE segment_files SET size=?, packets=?, current=0, closed=julianday('now') WHERE segment=?", [file_offset, file_num_packets, current_segment]);
    if ( ! segment_file )  {
        process.exit(0)
    }
    fs.closeSync(segment_file)
    process.exit();
/*
    segment_file.close( async (err) => {  
        if ( err ) { 
            console.log(`Error closing segment data file: ${err}`) 
        }
        err = await tracking_db.close()
        if ( err ) { console.log(`Error closing tracking database: ${err}`) }
        process.exit();
    })
*/
   
});
