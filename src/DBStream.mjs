'use strict';

import { Readable, Transform } from 'stream'
import fs from 'fs'
import sqlite3 from 'sqlite3'
import { open as dbopen } from 'sqlite'
import { tracking_db_file, get_segment_filename } from './tracking-db.js'


export class OffsetsLookupStream extends Transform {

    constructor( uid, opts={} ) {
        super( {objectMode: true} )
        this.uid = uid
        this.stmt = null
        this.db = {}
        //this.on( 'end', () => this.stmt.finalize( () => this.db.close() ));
console.log('Created an offsets lookup stream')
    }

    async _transform (record, enc, cb) {
//console.log(`offsetslookup stream got a record: ${JSON.stringify(record)}`)
        let strm = this;

        let hex_uid = Buffer.from(this.uid).toString('hex')
        let sql = 'SELECT * FROM packets WHERE uid=? ORDER BY segment,offset ASC'

        if ( ! this.db[record.segment] ) {
            let segment_file = get_segment_filename(record.segment) + ".db";
            //fs.exists(segment_file, () => { 
            //    cb();
            //})

console.log(`opening a segment file database: ${segment_file}`)
            this.db[record.segment] = await dbopen({filename: segment_file, driver: sqlite3.Database, mode: sqlite3.OPEN_READONLY})
console.log(`opening a segment db: ${sql}`)
            //this.stmt = await this.db[record.segment].prepare(sql)
        }
        
        let rowCount = await this.db[record.segment].each(sql, [this.uid], async function(err, result) {
        //await this.db.each( sql, [this.uid], await function(err, result) {
        //const rowCount = this.db[record.segment].getDatabaseInstance().each(sql, [], (err, result) => {
//console.log(`got something? ${JSON.stringify(result)} `)
            if ( err ) {
                strm.emit('error', err )
            } else {
                if ( result ) {
//console.log(`pushing a result from the offset looku0p stream ${JSON.stringify(result)}`)
                    if ( ! strm.push(result) ) {
//                        console.log("getting pushback!!")
                    }
                } else {
console.log(`close db segment ${record.segment}`)
                    // If this has been fully retrieved, remove the stmt and call the transform callback
                    await this.db[record.segment].close()
                    delete this.db[record.segment]
                    cb()
                }
	    }
        })
       // cb()
        //console.log(`Read ${rowCount} rows`)
        //cb(null)
	//console.log("called the callback")
    }
}

export class DBStream extends Readable {

    constructor( opts ) {
        super( { objectMode: true } );
        this.stmt = opts.db.prepare( opts.sql );
        //this.on( 'end', () => this.stmt.finalize( () => this.db.close() ));
    }

    _read() {
        let strm = this;
        this.stmt.get( function(err,result) {

            // If result is undefined, push null, which will end the stream.
            /* 
             * Should have no backpressure problems, 
             * since _read is only called when the downstream is 
             * ready to fetch data
             */
            err ?
                strm.emit('error', err ) :
		strm.push(result || null)

       })
    }

}

