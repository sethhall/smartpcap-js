'use strict';

import url from 'url'
import http from 'http'
import fs from 'fs'
import stream from 'stream'
import * as Parser from 'binary-parser'
import express from 'express'
import config from 'config'
import byt from 'byt'
import prettyBytes from 'pretty-bytes'

import sqlite3 from 'sqlite3'
import { open as dbopen } from 'sqlite'
import { OffsetsLookupStream, DBStream } from './DBStream.mjs';
import { get_latest_offsets_db, tracking_db_file, get_segment_filename, get_offsets_db } from './tracking-db.js'

const Frame = new Parser.Parser()
  .endianess("little")
  .uint32("ts_secs")
  .uint32("garbage1")
  .uint32("ts_usecs")
  .uint32("garbage2")
  .uint32("snap_len")
  .uint32("orig_len")
  .buffer("packet", { length: "orig_len" })
  .saveOffset("currentOffset")


const Packets = new Parser.Parser()
  .array("packets", {type: Frame,
                     readUntil: "eof"})

const packet_header = Buffer.from('1MOyoQIABAAAAAAAAAAAAP//AAABAAAA', 'base64')

var tracking_db = await dbopen({filename: tracking_db_file, driver: sqlite3.Database, mode: sqlite3.OPEN_READONLY})

var open_file_cache = {}

function convert_to_pcap(buf, ret_buf) {
    let offset = 0
    let p = null
//console.log(`converting to pcap - ${buf.length}`)
    while ( offset < buf.length ) {
        try {
            //parsed = Packets.parse(buf)
        p = Frame.parse(buf)
        } catch (err) {
            console.log("not enough data to parse everything? " + err)
            console.log(JSON.stringify(buf))
        }

        const nums = new Uint32Array([p.ts_secs, p.ts_usecs, p.snap_len, p.orig_len])
        const pkt_hdr = new Uint8Array(nums.buffer)
        Buffer.from(pkt_hdr).copy(ret_buf, offset)
        offset += 16
        p.packet.copy(ret_buf, offset)
        offset += p.packet.length
	}

    //}
    return ret_buf.slice(0,offset)
}

function read_file_data(row, buf) {
  //console.log(`reading segment: ${row.segment} == reading offset:${row.offset} == length:${row.length}`)

  // open file reference counting
  if ( ! open_file_cache[row.segment] ) {
    //console.log(`open a new file segment: ${row.segment}`)
    // convert segment offset to filename
    let filename = get_segment_filename(row.segment)
    open_file_cache[row.segment] = { file: fs.openSync(filename, 'r', 0o666),
                                     refs: 1 };
  } else {
    open_file_cache[row.segment].refs++
  }
  const pcap_f = open_file_cache[row.segment].file
try {
  const read_bytes = fs.readSync(pcap_f, buf, {offset: 0, length: row.length, position: row.offset})
} catch (err) {
  console.log(`failed to read data: ${err}`)
}
//console.log(`read a chunk ${JSON.stringify(buf.slice(0,row.length))}`)
console.log(`read a chunk offset: ${row.offset}  length: ${row.length}`)
  return buf.slice(0,row.length)
}


const app = new express;
const PORT = config.get("frontend.port");

app.use(express.json());
app.listen(PORT, () => console.log(`Express server currently running on port ${PORT}`));


app.get('/download/:uid', async (req, res) => {
  console.log('Got uid request!!!!!!!!')
  res.writeHead(200, {
        'Content-Type': 'application/octet-stream',
	'Content-Disposition': `attachment; filename="${req.params.uid}.pcap"`,
        'Transfer-Encoding': 'chunked',
  })

  res.write(packet_header)

  // Allocate these externally and just use them over and over to avoid tons of transmission time allocation.
  var inbuf = Buffer.allocUnsafe(100 * 1024)
  var outbuf = Buffer.allocUnsafe(100 * 1024)
  try {
    await stream.pipeline([
      // Lookup all hot segment files
      new DBStream( { db: tracking_db.getDatabaseInstance(), sql: "SELECT segment FROM segment_files WHERE hot=1 AND segment>0 ORDER BY segment ASC" } ),
      // Transform segment files into segment/offset/length
      new OffsetsLookupStream(req.params.uid),
      // FIXME!!! This is an sql injection!!
      //new DBStream( {db: db.db, sql: `SELECT * FROM packets WHERE uid='${req.params.uid}' ORDER BY segment,offset ASC`} ),
	    
//      new stream.Transform({ objectMode: true, transform:( row, enc, cb ) => cb( null, JSON.stringify(row))}), 

      // Take index info from db and turn it into file data...
      new stream.Transform({ writableObjectMode: true, readableObjectMode: false, transform:( row, enc, cb ) => cb( null, read_file_data(row, inbuf))}), 
      // Rewrite the data from disk to turn it into *real* on-disk pcap format
      new stream.Transform({ objectMode: false, transform:( data, enc, cb ) => cb( null, convert_to_pcap(data, outbuf))}), 
      // Write data to the user...
      res],
      (err) => { console.log(`done streaming ${req.params.uid}`); console.log(err); res.end() })
  } catch (err) {
    res.end(`There was a problem retrieving the pcap.  Try again soon (${err})`)
  }
});

app.get('/sample/', async (req, res) => {
  let db = await get_latest_offsets_db(tracking_db)
  const rowsCount = await db.each('SELECT uid, SUM(num_pkts) as total_packets, SUM(length) AS total_length FROM packets GROUP BY uid ORDER BY total_length DESC LIMIT 1000;', [], (err, row) => {
    // Add 24 bytes for the pcap header and remove 8 bytes per packet due to the 64bit->32bit timestamp issue
    let pcap_size = row.total_length - (row.total_packets*8) + 24
    let uid = row.uid
    res.write(`<a href="/download/${uid}">${uid}</a> => packets: ${row.total_packets} :: pcap_size: ${pcap_size}<br/>`)
  },
  () => { res.end() })
});

app.get('/', async (req, res) => {
    const rowsCount = await tracking_db.each('SELECT strftime("%Y-%m-%d %H:%M:%S", MIN(opened)) AS oldest, SUM(packets) as packets, SUM(size) AS size FROM segment_files WHERE hot=1;', [], (err, row) => {
        res.end(`${prettyBytes(row.size)} of data in ${row.packets} packets as far back as ${row.oldest}`)
    })
})

