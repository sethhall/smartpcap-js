import config from 'config'
import path from 'path'
import sqlite3 from 'sqlite3'

const SEGMENT_NAME = "smartpcap.segment"
export const tracking_db_file = path.format({dir: config.get("storage.data_dir"), name: 'tracking', ext: '.db'})

export async function find_first_segment(tracking_db) {
    let row = await tracking_db.get('SELECT MAX(segment) as segment FROM segment_files', [])
    return (row.segment) ? row.segment : 0
}

export function get_segment_filename(segment) {
    return path.format({dir: config.get("storage.data_dir"), name: SEGMENT_NAME, ext: "." + segment})
}

export function get_offsets_db(segment, init=false) {
    let segment_db_file = get_segment_filename(segment) + '.db'
    let offset_db = new sqlite3.Database(segment_db_file)
    if ( init ) {
        offset_db.serialize( () => {
            offset_db.run('PRAGMA JOURNAL_MODE = WAL;');
            offset_db.run('PRAGMA SYNCHRONOUS = NORMAL;');
            offset_db.run('CREATE TABLE IF NOT EXISTS packets (uid TEXT, segment INT, offset INT, length INT, num_pkts INT);')
        });
    }
    return offset_db
}

export async function get_latest_offsets_db(tracking_db) {
    let segment = await find_first_segment(tracking_db)
    return get_offsets_db(segment)
}
