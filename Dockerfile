FROM node:16.13.1-alpine3.15

COPY src/* /smartpcap/

COPY package* /smartpcap/
RUN cd /smartpcap && \
    npm install

WORKDIR /smartpcap
ENTRYPOINT ["node", "esm.mjs"]
